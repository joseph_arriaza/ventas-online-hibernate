CREATE TABLE Usuario(
  idUsuario INT NOT NULL PRIMARY KEY,
  username VARCHAR(250) NOT NULL,
  pass VARCHAR(250) NOT NULL,
  nombre VARCHAR(250) NOT NULL,
  tarjetaCredito VARCHAR(250) NOT NULL,
  imagen VARCHAR(250) NOT NULL,
  rol VARCHAR(250) NOT NULL
);

CREATE SEQUENCE usuario_seq;
CREATE OR REPLACE TRIGGER usuario_bir 
BEFORE INSERT ON Usuario
FOR EACH ROW

BEGIN
  SELECT usuario_seq.NEXTVAL
  INTO   :new.idUsuario
  FROM   dual;
END;

INSERT INTO Usuario VALUES
(1,'juan','juan','juan','1234567890123','imagen.png','Rol.Admin');
INSERT INTO Usuario VALUES
(2,'juana','juana','juana','1234567890123','imagen.png','Rol.Admin');

CREATE TABLE Producto(
  idProducto INT NOT NULL PRIMARY KEY,
  precio INT NOT NULL,
  cantidadDisponible INT NOT NULL,
  nombreProducto VARCHAR(255) NOT NULL,
  descricion VARCHAR(255) NOT NULL,
  rutaImagen VARCHAR(255) NOT NULL
);

CREATE SEQUENCE producto_seq;
CREATE OR REPLACE TRIGGER producto_bir 
BEFORE INSERT ON Producto
FOR EACH ROW

BEGIN
  SELECT producto_seq.NEXTVAL
  INTO   :new.idProducto
  FROM   dual;
END;

CREATE TABLE Carrito(
  idCarrito INT NOT NULL PRIMARY KEY,
  idUsuario INT NOT NULL,
  precioTotal INT NOT NULL,
  idProducto VARCHAR(255) NOT NULL,
  cantProducto VARCHAR(255) NOT NULL,
  FOREIGN KEY(idUsuario) REFERENCES Usuario
);

CREATE SEQUENCE carrito_seq;
CREATE OR REPLACE TRIGGER carrito_bir 
BEFORE INSERT ON Carrito
FOR EACH ROW

BEGIN
  SELECT carrito_seq.NEXTVAL
  INTO   :new.idCarrito
  FROM   dual;
END;
select * from usuario