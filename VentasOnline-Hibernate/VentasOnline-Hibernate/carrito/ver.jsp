<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.jarriaza.beans.Producto" %>
<%@ page import="org.jarriaza.beans.Carrito"  %>
<%@ page import="org.jarriaza.beans.Usuario"  %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="assets/css/style.css"/>
<meta charset="UTF-8">
<title>Ventas Online</title>
</head>
<body>
	<center>
		<jsp:include page="../include/sesion.jsp"></jsp:include>
			<jsp:include page="../complements/header.jsp"></jsp:include>
			<div id="login">					
						<br><br>
						<img src="http://localhost:8082/VentasOnline/imagenes/usuario/default.jpg" width=100px height=100px />
						<div>Bienvenido ${usuario.getNombre()}</div>
						<div style="text-align:right;text-decoration:none;"><a href="ServletDesautenticar.do">Logout</a></div>			
			</div>
			<div id="content">
				<br><br>
					<% HttpSession miSesion=request.getSession();
					Usuario usr =(Usuario) miSesion.getAttribute("usuario");
					if(usr.getRol().equals("Rol.Usuario")){%>							
						<h2>Carrito ${username}</h2>
						<table border=1>
							<thead>
								<tr>
									<th  style="border-width:0px;" width="40px" ></th>
									<th width="150px">Producto</th>
								    <th width="150px">Cantidad</th>
								    <th width="150px">Precio Unitario</th>
								    <th width="150px">Precio Cantidad</th>
								</tr>
							</thead>
							<c:forEach var="productoCarr" items="${lista}">
								<tbody align="center">
									<tr>
									  <td style="border-width:0px;"><a href="ServletDelProCart.do?idProducto=${productoCarr.getIdProducto()}&estadoCarr=false"><img src="assets/img/menos.png"width="35px" height="20px" title="Comprar"></a></td>
								      <td>${productoCarr.getNombreProducto()}</td>
								      <td>${productoCarr.getCantidadDisponible()}</td>
								      <td>Q${productoCarr.getPrecio()}</td>
								      <td><script>document.write('Q'+'${productoCarr.getCantidadDisponible()}'*'${productoCarr.getPrecio()}');</script></td>
								      
								    </tr>
								</tbody>						
							</c:forEach>
							<tfoot align="center">
								<tr>
									<td style="border-width:0px;"></td>
								    <td></td>
								    <td></td>
								    <td></td>
									<td id='total'>Total: Q${precioTotal}</td>
								</tr>
							</tfoot>
						</table>										
						<br><br>
						
						<br><br><br><br>
						<script>	
							if(!document.getElementById("total").textContent.contains("Total: Q0") && document.getElementById("total").textContent!="Total: Q"){
								document.write('<a href="ServletCompraCarrito.do?estado=true"><img align="right"src="assets/img/carrito.png"width="60px" height="50px" title="Comprar"></a>');
							}else{
								document.write('<img align="right"src="assets/img/carrito.png"width="60px" height="50px" title="No hay articulos para comprar">');
							}
						</script>
					<% }%>						
				<br><br><br>
			</div>

		<br><br><br>
	</center> 
</body>
</html>