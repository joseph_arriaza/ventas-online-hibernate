<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.jarriaza.beans.Producto" %>
<%@ page import="org.jarriaza.beans.Usuario" %>
<%@ page session="true" %> 
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="assets/css/style.css"/>
<meta charset="UTF-8">
<title>Ventas Online</title>
</head>
<body>
	<center>
		<jsp:include page="../complements/header.jsp"></jsp:include>
			<div id="login">
					<% HttpSession miSesion=request.getSession();
					Usuario usr =(Usuario) miSesion.getAttribute("usuario");
					if(usr!=null){%>						
						<br><br>
						<img src="http://localhost:8082/VentasOnline/imagenes/usuario/default.jpg" width=100px height=100px />
						<div>Bienvenido ${usuario.getNombre()}</div>
						<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline/ServletDesautenticar.do">Logout</a></div>
					<% }else{%>
						<br>
						<form action="http://localhost:8082/VentasOnline/ServletAutenticar.do" method="GET">
						<br>Usuario:<br>
						<input type="text" name="txtUsuario"  required>
						<br>Contraseņa:<br>
						<input type="password" name="txtPassword" required>
						<br>
						<input type="submit" value="Login">
						</form>
						<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline/usuario/registrar.jsp">Registrate</a></div>
					<% }%>				
			</div>
			<div id="content">
				<br><br><br><br><br>
				<% int x=-60; double y=-24;%>
				<c:forEach var="productoBuscar" items="${listaBuscar}">
					<% if(x==-60){%>
						<div style='height:210px;width:180px;border-style:solid;border-width: 1px;border-color: #ccc;margin-left:<%= x%>%;'>									
					<% }else if(x==0){%>
						<div style='height:210px;width:180px;border-style:solid;border-width: 1px;border-color: #ccc;margin-left:<%= x%>%;margin-top:<%= y%>%;'>						
					<% }else if(x==60){%>	
						<div style='height:210px;width:180px;border-style:solid;border-width: 1px;border-color: #ccc;margin-left:<%= x%>%;margin-top:<%= y%>%;'>
						<% x=-120;%>
					<% }%>
						${productoBuscar.getNombreProducto()}<br>
						<img src="imagenes/producto/producto.jpg" title="${productoBuscar.getDescripcion()}" width=125px height=130px /><br>	
						<font color="#ccc">Precio: </font>${productoBuscar.getPrecio()}<br>
						<% if(usr!=null){%>
							<% if(usr.getRol().equals("Rol.Admin")){%>
								<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline/ServletEditProducto.do?idProducto=${productoBuscar.getIdProducto()}">Editar </a></div>
								<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline/ServletDelProducto.do?idProducto=${productoBuscar.getIdProducto()}">Eliminar</a></div>
							<% }else if(usr.getRol().equals("Rol.Usuario")){%>
								<script>
									if("${estadoCarrito}"==""){
										document.write('<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline/ServletAddCart.do?idProducto=${productoBuscar.getIdProducto()}&estadoCarr=true&pag=buscar&nombreP=${palabraClave}"><img src="assets/img/carritoAdd.png" width="40px" height="30px" title="Agregar al carrito"/></a></div>');
									}else{
										document.write('<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline/ServletAddCart.do?idProducto=${productoBuscar.getIdProducto()}&estadoCarr=false&pag=buscar&nombreP=${palabraClave}"><img src="assets/img/carritoAdd.png" width="40px" height="30px" title="Agregar al carrito"/></a></div>');
									}
								</script>
							<% }%>						
						<% }%>																	
					</div>
					<br><br>
					<% x+=60;y=-43; %>			
				</c:forEach>
				<br><br><br><br><br><br><br>
			</div>
		<br><br><br>
	</center> 
</body>
</html>