<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>l" %>
<%@ page import="org.jarriaza.beans.Usuario" %>
<%@ page import="org.jarriaza.beans.Producto" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../assets/css/style.css"/>
<link rel="stylesheet" href="assets/css/style.css"/>
<meta charset="UTF-8">
<title>Ventas Online</title>
</head>
<body>
	<center>
	<jsp:include page="../include/sesion.jsp"></jsp:include>
		<div id="head" style='position:fixed; top:0; right:0;'>
			<div id="menu" style='position:fixed'>
				<ul class="nav">						
						<script>
							if("${estadoAddU}"!=""){
								document.write('<li><a href="index.jsp">Inicio</a></li>');
							}else{
								document.write('<li><a href="../index.jsp">Inicio</a></li>');	
							}									
						</script>							
						<li><a href="#">Usuarios</a>
							<ul class="subs">
								<script>
									if("${estadoAddU}"!=""){
										document.write('<li style="border-top:1px solid white;"><a href="usuario/listar.jsp">Ver Usuarios</a></li>');
									}else{
										document.write('<li style="border-top:1px solid white;"><a href="../usuario/listar.jsp">Ver Usuarios</a></li>');	
									}									
								</script>	
								<li style="border-top:1px solid white;"><a href="#">Nuevo Usuario</a></li>								
							</ul>
						</li>
						<li><a href="#">Producto</a>
							<ul class="subs">
								<script>
									if("${estadoAddU}"!=""){
										document.write('<li style="border-top:1px solid white;"><a href="producto/agregar.jsp">Nuevo Producto</a></li>');
									}else{
										document.write('<li style="border-top:1px solid white;"><a href="../producto/agregar.jsp">Nuevo Producto</a></li>');	
									}									
								</script>								
							</ul>
						</li>
				</ul>
			</div>
		
			<div id="search" style='position:fixed;  right:0;'>
				<script>
					if("${estadoAddU}"!=""){
						document.write('<form method="GET" action="ServletBuscar.do">');
					}else{
						document.write('<form method="GET" action="../ServletBuscar.do">');	
					}									
				</script>
					<input type="text" name="txtBuscar" placeholder="Producto a buscar..." size="45" style='height:30px;' required>
					<input type="submit" value="Buscar">
				</form>	
			</div>
		</div>
			<div id="login">					
						<br><br>
						<img src="http://localhost:8082/VentasOnline/imagenes/usuario/default.jpg" width=100px height=100px />
						<div>Bienvenido ${usuario.getNombre()}</div>
						<br>
						<script>
							if("${estadoAddU}"!=""){
								document.write('<div style="text-align:right;text-decoration:none;"><a href="ServletDesautenticar.do">Logout</a></div>');
							}else{
								document.write('<div style="text-align:right;text-decoration:none;"><a href="../ServletDesautenticar.do">Logout</a></div>');	
							}									
						</script>			
			</div>

			<div id="content">
				<br><br><br><br><br>
				<% int x=-60; double y=-24;
				HttpSession miSesion=request.getSession();
				Usuario usr =(Usuario) miSesion.getAttribute("usuario");%>
					<% if(usr.getRol().equals("Rol.Admin")){%>					
						<h2>Agregar Usuario</h2>
						<script>
							if("${estadoAddU}"!=""){
								document.write('<form method="post" action="ServletAgregarUsuario.do">');
							}else{
								document.write('<form method="post" action="../ServletAgregarUsuario.do">');	
							}									
						</script>
							<br>Username:<br>
							<input type="text" name="txtUsername" size="30" required>							
							<br>Password:<br>
							<input type="password" name="txtPass" size="30" required>
							<br>Nombre:<br>
							<input type="text" name="txtNombre" value="${nombre}" size="30" required>
							<br>Tarjeta de Credito:<br>
							<input type="text" name="txtTarjeta" value="${tarjeta}" size="30" required>
							<br>Imagen:<br>
							<input type="text" name="txtImagen" value="${imagen}" size="30" required>
							<br>Rol:<br>
							<select name="txtRol" style='width:200px;text-align:center;' required>
								<option value="1">Admin</option>
								<option value="2">Usuario</option>
							</select>
							<br><br>
							<input type="submit" value="Agregar">
						</form>
					
					<% }%>			
				<br><br><br><br><br><br><br>
			</div>
		<br><br><br>
		<script>
			if("${estadoAddU}"!=""){
				alert("${estadoAddU}");
			}								
		</script>
	</center> 
</body>
</html>