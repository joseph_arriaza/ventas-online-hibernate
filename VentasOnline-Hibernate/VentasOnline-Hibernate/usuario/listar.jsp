<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.jarriaza.beans.Producto" %>
<%@ page import="org.jarriaza.beans.Usuario" %>
<%@ page import="org.jarriaza.db.Conexion" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="http://localhost:8082/VentasOnline-Hibernate/assets/css/style.css"/>
<meta charset="UTF-8">
<title>Ventas Online</title>
</head>
<body>
	<center>
	<jsp:include page="../include/sesion.jsp"></jsp:include>
		<div id="head" style='position:fixed; top:0; right:0;'>
			<div id="menu" style='position:fixed'>
				<ul class="nav">						
						<li><a href="../index.jsp">Inicio</a>
						</li>
						<li><a href="#">Usuarios</a>
							<ul class="subs">
								<li style='border-top:1px solid white;'><a href="#">Ver Usuarios</a></li>
								<li style="border-top:1px solid white;"><a href="http://localhost:8082/VentasOnline-Hibernate/usuario/nuevo.jsp">Nuevo Usuario</a></li>
							</ul>
						</li>
						<li><a href="#">Producto</a>
							<ul class="subs">
								<li style="border-top:1px solid white;"><a href="http://localhost:8082/VentasOnline-Hibernate/producto/agregar.jsp">Nuevo Producto</a></li>							
							</ul>
						</li>
				</ul>
			</div>
		
			<div id="search" style='position:fixed;  right:0;'>
				<form method="GET" action="http://localhost:8082/VentasOnline-Hibernate/ServletBuscar.do">
					<input type="text" name="txtBuscar" placeholder="Producto a buscar..." size="45" style='height:30px;' required>
					<input type="submit" value="Buscar">
				</form>	
			</div>
		</div>
			<div id="login">
					<% HttpSession miSesion=request.getSession();
					Usuario usr =(Usuario) miSesion.getAttribute("usuario");%>					
						<br><br>
						<img src="http://localhost:8082/VentasOnline-Hibernate/imagenes/usuario/default.jpg" width=100px height=100px />
						<div>Bienvenido ${usuario.getNombre()}</div>
						<br>
						<script>
							if("${estadoDel}"!=""){
								document.write('<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline-Hibernate/ServletDesautenticar.do">Logout</a></div>');
							}else{
								document.write('<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline-Hibernate/ServletDesautenticar.do">Logout</a></div>');	
							}									
						</script>			
			</div>

			<div id="content">
				<br><br><br><br><br>
				<% int x=-60; double y=-24;%>
					<% if(usr.getRol().equals("Rol.Admin")){%>
						<% ArrayList<Usuario> lista=new ArrayList<Usuario>();
							for(Object o:Conexion.getInstancia().hacerConsulta("From Usuario")){
								lista.add((Usuario)o);
							}
						%>					
						<c:forEach var="usuarioBuscar" items="<%=lista%>">
							<% if(x==-60){%>
								<div style='height:210px;width:180px;border-style:solid;border-width: 1px;border-color: #ccc;margin-left:<%= x%>%;'>									
							<% }else if(x==0){%>
								<div style='height:210px;width:180px;border-style:solid;border-width: 1px;border-color: #ccc;margin-left:<%= x%>%;margin-top:<%= y%>%;'>						
							<% }else if(x==60){%>	
								<div style='height:210px;width:180px;border-style:solid;border-width: 1px;border-color: #ccc;margin-left:<%= x%>%;margin-top:<%= y%>%;'>
								<% x=-120;%>
							<% }%>	
								${usuarioBuscar.getUsername()}<br>
								<font color="#ccc">Rol: </font>${usuarioBuscar.getRol()}<br>
								<img src="http://localhost:8082/VentasOnline/imagenes/usuario/default.jpg" title="${usuarioBuscar.getNombre()}" width=125px height=120px /><br>
								<script>
									if("${estadoDel}"!=""){
										document.write('<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline-Hibernate/ServletEditUsuario.do?idUsuario=${usuarioBuscar.getIdUsuario()}">Editar </a></div>');
										document.write('<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline-Hibernate/ServletDelUsuario.do?idUsuario=${usuarioBuscar.getIdUsuario()}">Eliminar</a></div>');
									}else{
										document.write('<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline-Hibernate/ServletEditUsuario.do?idUsuario=${usuarioBuscar.getIdUsuario()}">Editar </a></div>');
										document.write('<div style="text-align:right;text-decoration:none;"><a href="http://localhost:8082/VentasOnline-Hibernate/ServletDelUsuario.do?idUsuario=${usuarioBuscar.getIdUsuario()}">Eliminar</a></div>');
									}									
								</script>																							
							</div>
							<br><br>
							<% x+=60;y=-31.8; %>			
						</c:forEach>
					
					<% }%>	
				<br><br><br><br><br><br><br>
			</div>
		<br><br><br>
		<script>
			if("${estadoDel}"!=""){
				alert("${estadoDel}");
			}								
		</script>
	</center> 
</body>
</html>