<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.jarriaza.beans.Usuario" %>
<%@ page session="true" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Ventas Online</title>
</head>
<body>


	<%
		HttpSession miSesion=request.getSession();
		Usuario user = (Usuario)miSesion.getAttribute("usuario");
	%>
	<div id="head" style='position:fixed; top:0; right:0;'>
			<div id="menu" style='position:fixed'>
				<ul class="nav">						
					<%if(user!=null){%>
						<li><a href="index.jsp">Inicio</a>
						</li>
						<li><a href="#">Usuarios</a>
								<ul class="subs">
									<li style='border-top:1px solid white;'><a href="usuario/listar.jsp">Ver Usuarios</a></li>
									<li style='border-top:1px solid white;'><a href="usuario/nuevo.jsp">Nuevo Usuario</a></li>
								</ul>
							</li>
							<li><a href="#">Producto</a>
								<ul class="subs">
									<li style='border-top:1px solid white;'><a href="producto/agregar.jsp">Nuevo Producto</a></li>
								</ul>
							</li>
						<%}%>
				</ul>
						
			</div>
			<div id="search" style='position:fixed;  right:0;'>
				<form method="GET" action="ServletBuscar.do">
					<input type="text" name="txtBuscar" placeholder="Producto a buscar..." size="45" style='height:30px;' required>
					<input type="submit" value="Buscar">
				</form>	
			</div>
		</div>
</body>
</html>