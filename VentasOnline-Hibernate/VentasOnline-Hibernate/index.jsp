<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.jarriaza.beans.Producto" %>
<%@ page import="org.jarriaza.beans.Usuario" %>
<%@ page import="org.jarriaza.beans.Carrito"  %>
<%@ page import="org.jarriaza.db.Conexion"  %>
<%@ page import="java.util.*" %>
<%@ page session="true" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="assets/css/style.css"/>
</head>
<body>
	<center>
		<jsp:include page="complements/header.jsp"></jsp:include>
		
			<div id="login">
				<br><br>
				<% HttpSession miSesion=request.getSession();
				   Usuario usr =(Usuario) miSesion.getAttribute("usuario"); 
				if(usr!=null){%>
					<img src="${ruta}imagenes/usuario/default.jpg" width=100px height=100px />
					<div>Bienvenido ${usuario.getNombre()}</div>
					<div style="text-align:right;text-decoration:none;"><a href="ServletDesautenticar.do">Logout</a></div>
				<% }else{%>
						<br>
						<form action="ServletAutenticar.do" method="GET">
						<script>
							document.write('<img src="'+ruta[0]+"/"+ruta[1]+"/"+ruta[2]+"/"+ruta[3]+"/"+'imagenes/usuario/default.jpg" width=100px height=100px />');
						</script>
						<br>Usuario:<br>
						<input type="text" name="txtUsuario"  required>
						<br>Contraseņa:<br>
						<input type="password" name="txtPassword" required>
						<br>
						<input type="submit" value="Login">
						</form>
						<br>
						<div style="text-align:right;text-decoration:none;"><a href="usuario/registrar.jsp">Registrate</a></div>
					<% }%>				
			</div>
			<div id="content">
				<br><br><br><br><br>
				<%int x=-60; double y=-24;%>
				<% ArrayList<Producto> lista=new ArrayList<Producto>();
							for(Object o:Conexion.getInstancia().hacerConsulta("From Producto")){
								lista.add((Producto)o);
							}
						%>	
				<c:forEach var="productoBuscar" items="<%= lista%>">
					<% if(x==-60){%>
						<div style='height:210px;width:180px;border-style:solid;border-width: 1px;border-color: #ccc;margin-left:<%= x%>%;'>									
					<% }else if(x==0){%>
						<div style='height:210px;width:180px;border-style:solid;border-width: 1px;border-color: #ccc;margin-left:<%= x%>%;margin-top:<%= y%>%;'>						
					<% }else if(x==60){%>	
						<div style='height:210px;width:180px;border-style:solid;border-width: 1px;border-color: #ccc;margin-left:<%= x%>%;margin-top:<%= y%>%;'>
						<% x=-120;%>
					<% }%>	
						${productoBuscar.getNombreProducto()}<br>
						<img src="imagenes/producto/producto.jpg" title="${productoBuscar.getDescripcion()}" width=125px height=120px /><br>	
						<font color="#ccc">Precio: </font>${productoBuscar.getPrecio()}
							<% if(usr!=null){%>
								<% if(usr.getRol().equals("Rol.Admin")){%>
									<div style="text-align:right;text-decoration:none;"><a href="ServletEditProducto.do?idProducto=${productoBuscar.getIdProducto()}">Editar </a></div>
									<div style="text-align:right;text-decoration:none;"><a href="ServletDelProducto.do?idProducto=${productoBuscar.getIdProducto()}">Eliminar</a></div>
								<% }else if(usr.getRol().equals("Rol.Usuario")){%>
									<script>
										if("${estadoCarrito}"==""){
											document.write('<div style="text-align:right;text-decoration:none;"><a href="ServletAddCart.do?idProducto=${productoBuscar.getIdProducto()}&estadoCarr=true&pag=index"><img src="assets/img/carritoAdd.png" width="40px" height="30px" title="Agregar al carrito"/></a></div>');
										}else{
											document.write('<div style="text-align:right;text-decoration:none;"><a href="ServletAddCart.do?idProducto=${productoBuscar.getIdProducto()}&estadoCarr=false&pag=index"><img src="assets/img/carritoAdd.png" width="40px" height="30px" title="Agregar al carrito"/></a></div>');
										}
									</script>
								<% }%>																							
							<% }%>
					</div>
					<br><br>
					<% x+=60;y=-31.8;/*y=-29.4  -31.8;  y=-43*/ %>			
				</c:forEach>
				<br><br><br><br><br><br><br>
			</div>
		
		<script>
			if("${estado}"!="")
				alert("${estado}");
			if("${estadoAdd}"!="")
				alert("Producto agregado correctamente!!!");
			if("${estadoEdit}"!="")
				alert("Producto editado correctamente!!!");
			if("${estadoAddU}"!="")
				alert("Usuario agregado correctamente!!!");
			if("${estadoPDF}"!=""){
				alert("${estadoPDF}");
			}
		</script>
		<br><br><br>
	</center> 
</body>
</html>