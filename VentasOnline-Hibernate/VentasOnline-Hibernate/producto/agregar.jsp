<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>o" %>
<%@ page import="org.jarriaza.beans.Producto" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../assets/css/style.css"/>
<meta charset="UTF-8">
<title>Ventas Online</title>
</head>
<body>
	<center>
	<jsp:include page="../include/sesion.jsp"></jsp:include>
		<div id="head" style='position:fixed; top:0; right:0;'>
			<div id="menu" style='position:fixed'>
				<ul class="nav">						
						<li><a href="../index.jsp">Inicio</a></li>
						<li><a href="#">Usuarios</a>
							<ul class="subs">
								<li style='border-top:1px solid white;'><a href="../usuario/listar.jsp">Ver Usuarios</a></li>
								<li style='border-top:1px solid white;'><a href="../usuario/nuevo.jsp">Nuevo Usuario</a></li>
							</ul>
						</li>
						<li><a href="#">Producto</a>
							<ul class="subs">
								<li style='border-top:1px solid white;'><a href="#">Nuevo Producto</a></li>
							</ul>
						</li>
				</ul>
			</div>
			<div id="search" style='position:fixed;  right:0;'>
				<form method="GET" action="../ServletBuscar.do">
					<input type="text" name="txtBuscar" placeholder="Producto a buscar..." size="45" style='height:30px;' required>
					<input type="submit" value="Buscar">
				</form>	
			</div>
		</div>
		
			<div id="login">
						<br><br>
						<img src="http://localhost:8082/VentasOnline/imagenes/usuario/default.jpg" width=100px height=100px />
						<div>Bienvenido ${usuario.getNombre()}</div>
						<br>
						<div style="text-align:right;text-decoration:none;"><a href="../ServletDesautenticar.do">Logout</a></div>			
			</div>

			<div id="content">
				<br><br><br>
						<h2>Agregar Producto</h2>
						<form method="post" enctype="MULTIPART/FORM-DATA" action="../ServletUploadImage.do?tipo=producto">
							<br>Producto:<br>
							<input type="text" name="txtNombre" size="30" required>							
							<br>Precio:<br>
							<input type="number" name="txtPrecio" size="30" required>
							<br>Cantidad:<br>
							<input type="number" name="txtCantidad" size="30" required>
							<br>Imagen:<br>
							<input type="file" name="txtImagen" size="30" required/>
							<br>Descripcion:<br>
							<textarea name="txtDescripcion" rows="4" cols="22"required></textarea>
							<br>
							<input type="submit" value="Agregar">
						</form>	
				<br><br><br><br><br><br><br>
			</div>		
		<br><br><br>
	</center> 
</body>
</html>