<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.jarriaza.beans.Producto" %>
<%@ page import="org.jarriaza.beans.Usuario" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="assets/css/style.css"/>
<link rel="stylesheet" href="../assets/css/style.css"/>
<meta charset="UTF-8">
<title>Ventas Online</title>
</head>
<body>
	<center>
		<script>
			if("${idProducto}"==""){
				window.location='../index.jsp';
			}
		</script>
		<jsp:include page="../complements/header.jsp"></jsp:include>
			<jsp:include page="../include/sesion.jsp"></jsp:include>
			<div id="login">					
						<br><br>
						<img src="http://localhost:8082/VentasOnline/imagenes/usuario/default.jpg" width=100px height=100px />
						<div>Bienvenido ${usuario.getNombre()}</div>
						<div style="text-align:right;text-decoration:none;"><a href="ServletDesautenticar.do">Logout</a></div>				
			</div>
			<div id="content">
				<br><br><br>
					<% HttpSession miSesion=request.getSession();
					Usuario usr =(Usuario) miSesion.getAttribute("usuario");
					if(usr.getRol().equals("Rol.Admin")){%>
						<h2>Editar ${nombre}</h2>
						<form method="post" action="ServletEditProducto.do?idProducto=${idProducto}&editar=true">
							<input type="hidden" name="editar" value="true">
							<input type="hidden" name="txtId" value="${idProducto}">
							<br>Producto:<br>
							<input type="text" name="txtNombre" value="${nombre}" size="30" required>
							<br>Precio:<br>
							<input type="number" name="txtPrecio" value="${precio}" size="30"  required>
							<br>Cantidad:<br>
							<input type="number" name="txtCantidad" value="${cantidad}" size="30" required>
							<input type="hidden" name="txtImagen" value="${imagen}" size="30" required>
							<br>Descripcion:<br>
							<textarea name="txtDescripcion" rows="4" cols="22"required>${descripcion}</textarea>
							<br>
							<input type="submit" value="Editar">
						</form>
					<% }%>	
				<br><br><br><br><br><br><br>
			</div>
		<br><br><br>
	</center> 
</body>
</html>