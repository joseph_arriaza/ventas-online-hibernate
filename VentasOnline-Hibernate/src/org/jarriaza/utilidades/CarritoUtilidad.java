package org.jarriaza.utilidades;

import org.jarriaza.beans.Carrito;
import org.jarriaza.db.Conexion;

public class CarritoUtilidad {
	public static final CarritoUtilidad instancia=new CarritoUtilidad();
	
	public static Carrito carro;
	
	public Carrito NuevoCarrito(String estado){
		if(CarritoUtilidad.carro==null && estado.equals("true")){
			CarritoUtilidad.carro=new Carrito();
		}
		return carro;
	}
	
	public int lastId(){
		int id=0;
		for(Object o:Conexion.getInstancia().hacerConsulta("From Carrito")){
			Carrito carro=(Carrito)o;
			id=carro.getIdCarrito();
		}
		return id+1;
	}
}
