package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jarriaza.utilidades.CarritoUtilidad;

public class ServletDesautenticar extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws ServletException, IOException{
		doPost(peticion,respuesta);		
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException{
		peticion.removeAttribute("usuarioAutenticado");
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		CarritoUtilidad.carro=null;
		peticion.getSession(false);
		peticion.getSession().invalidate();
		peticion.removeAttribute("usuario");
		/*ManejadorProducto.descuento=null;*/
		despachador.forward(peticion,respuesta);
	}
}
