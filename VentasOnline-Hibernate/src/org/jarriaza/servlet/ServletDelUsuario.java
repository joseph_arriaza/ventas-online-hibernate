package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jarriaza.beans.Carrito;
import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;

public class ServletDelUsuario extends HttpServlet{

	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador;
		HttpSession miSesion=peticion.getSession(true);
		Usuario usr =(Usuario) miSesion.getAttribute("usuario");
		despachador=peticion.getRequestDispatcher("usuario/listar.jsp");
		
		try{
			if(Integer.parseInt(peticion.getParameter("idUsuario"))==usr.getIdUsuario()){
				peticion.setAttribute("estadoDel","No puedes eliminarte a ti mismo.");
			}else{
				for(Object o:Conexion.getInstancia().hacerConsulta("From Carro where idUsuario="+Integer.parseInt(peticion.getParameter("idUsuario")))){
					Carrito c=(Carrito)o;
					Conexion.getInstancia().eliminar(c);
				}
				Conexion.getInstancia().eliminar(Integer.parseInt(peticion.getParameter("idUsuario")));
				peticion.setAttribute("estadoDel","Usuario eliminado correctamente!!!");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		despachador.forward(peticion,respuesta);
	}
	
}
