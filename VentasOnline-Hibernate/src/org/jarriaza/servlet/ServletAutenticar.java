package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;

public class ServletAutenticar extends HttpServlet{
	
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador;
		if(Conexion.getInstancia().autenticarUsuario(peticion.getParameter("txtUsuario"), peticion.getParameter("txtPassword"))!=null){
			Usuario usr=Conexion.getInstancia().autenticarUsuario(peticion.getParameter("txtUsuario"), peticion.getParameter("txtPassword"));
			
			HttpSession sesion=peticion.getSession(true);
			sesion.setAttribute("usuario", usr);
			sesion.setAttribute("listaProductos", Conexion.getInstancia().hacerConsulta("From Producto"));
			if(usr.getRol()=="Rol.Admin"){
				sesion.setAttribute("listaUsuarios",Conexion.getInstancia().hacerConsulta("From Usuario"));
			}
			sesion.setAttribute("ruta", peticion.getAttribute("ruta"));
			despachador=peticion.getRequestDispatcher("index.jsp");
		}else{
			peticion.setAttribute("estado","Verifique sus credenciales.");
			despachador=peticion.getRequestDispatcher("index.jsp");
		}
		despachador.forward(peticion, respuesta);
	}

}
