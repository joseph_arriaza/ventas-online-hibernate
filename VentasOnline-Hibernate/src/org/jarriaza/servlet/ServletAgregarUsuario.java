package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;
public class ServletAgregarUsuario extends HttpServlet{
	
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");		
			despachador=peticion.getRequestDispatcher("index.jsp");
			peticion.setAttribute("estadoAddU","Agregado");
			Usuario userAdd=new Usuario();
			userAdd.setIdUsuario(0);
			System.out.println(userAdd.getIdUsuario());
			userAdd.setUsername(peticion.getParameter("txtUsername"));
			userAdd.setPassword(peticion.getParameter("txtPass"));
			userAdd.setNombre(peticion.getParameter("txtNombre"));
			userAdd.setTarjetaCredito(peticion.getParameter("txtTarjeta"));
			userAdd.setImagen(peticion.getParameter("txtImagen"));
			if(peticion.getParameter("txtRol").equals("1")){
				userAdd.setRol("Rol.Admin");
			}else{
				userAdd.setRol("Rol.Usuario");
			}
			Conexion.getInstancia().agregar(userAdd);
		
		despachador.forward(peticion,respuesta);
	}

}
