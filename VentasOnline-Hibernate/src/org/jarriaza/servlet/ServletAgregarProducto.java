package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jarriaza.beans.Producto;
import org.jarriaza.db.Conexion;

public class ServletAgregarProducto extends HttpServlet{

	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador;
		despachador=peticion.getRequestDispatcher("index.jsp");
		peticion.setAttribute("estadoAdd","true");
		Producto productoAdd=new Producto(0,
										  Integer.parseInt(peticion.getParameter("txtPrecio")),
										  Integer.parseInt(peticion.getParameter("txtCantidad")),
										  peticion.getParameter("txtNombre"),
										  peticion.getParameter("txtDescripcion"),
										  peticion.getParameter("txtImagen"));
		Conexion.getInstancia().agregar(productoAdd);
		peticion.setAttribute("estadoAdd","true");
		despachador.forward(peticion,respuesta);
	}
	
}
