package org.jarriaza.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class ServletUploadImage extends HttpServlet{

	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador =peticion.getRequestDispatcher("index.jsp");
		FileItemFactory file_factory = new DiskFileItemFactory();

        ServletFileUpload servlet_up = new ServletFileUpload(file_factory);
        List items = null;
		try {
			items = servlet_up.parseRequest(peticion);
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(peticion.getParameter("tipo").equals("producto")){
			String url="ServletAgregarProducto.do?";
			despachador=peticion.getRequestDispatcher("ServletAgregarProducto.do");
			for(int i=0;i<items.size();i++){
	            FileItem item = (FileItem) items.get(i);
	            if(item.getFieldName().equals("txtNombre")){
	            	url=url+"txtNombre="+item.getString();
	            }else if(item.getFieldName().equals("txtPrecio")){
	            	url=url+"&txtPrecio="+item.getString();
	            }else if(item.getFieldName().equals("txtCantidad")){
	            	url=url+"&txtCantidad="+item.getString();
	            }else if(item.getFieldName().equals("txtDescripcion")){
	            	url=url+"&txtDescripcion="+item.getString();
	            }else if (! item.isFormField()){
	            	String rutaTemp=this.getServletContext().getRealPath("");
	            	//E:\2013102\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\wtpwebapps\VentasOnline
	            	rutaTemp=rutaTemp.replace('\\','/');
	            	String rutaTempArr[]=rutaTemp.split("/");
	            	String nuevaRuta=rutaTempArr[0]+"\\"+rutaTempArr[1]+"\\"+rutaTempArr[rutaTempArr.length-1]+"\\"+rutaTempArr[rutaTempArr.length-1]+"\\imagenes\\producto\\";
	            	File archivo_server = new File(rutaTemp+item.getName());
	            	String ruta=item.getName();
	            	url=url+"&txtImagen="+ruta;
					try {
						item.write(archivo_server);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        }
			despachador=peticion.getRequestDispatcher(url);
		}
		
		despachador.forward(peticion,respuesta);
	}
	
}
