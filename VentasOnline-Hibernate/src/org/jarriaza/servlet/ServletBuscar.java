package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jarriaza.beans.Producto;
import org.jarriaza.db.Conexion;


public class ServletBuscar extends HttpServlet{
	
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);		
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		peticion.setAttribute("listaBuscar", Conexion.getInstancia().hacerConsulta("From Producto where nombreProducto LIKE '%"+peticion.getParameter("txtBuscar")+"%'"));
		peticion.setAttribute("palabraClave", peticion.getParameter("txtBuscar"));
		RequestDispatcher despachador=peticion.getRequestDispatcher("buscar/buscar.jsp");
		despachador.forward(peticion,respuesta);
	}

}
