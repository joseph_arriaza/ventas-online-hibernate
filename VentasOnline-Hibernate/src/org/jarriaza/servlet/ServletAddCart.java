package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jarriaza.beans.Carrito;
import org.jarriaza.utilidades.CarritoUtilidad;

public class ServletAddCart extends HttpServlet{
	
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");;
		try{
			if(peticion.getParameter("pag").equals("index")){
				despachador=peticion.getRequestDispatcher("index.jsp");
			}else{
				despachador=peticion.getRequestDispatcher("ServletBuscar.do?txtBuscar="+peticion.getParameter("nombreP"));
			}
			peticion.getParameter("idProducto");		
			peticion.setAttribute("estadoCarrito","false");
			Carrito carro=CarritoUtilidad.instancia.NuevoCarrito(peticion.getParameter("estadoCarr"));
			if(carro.getIdProducto()==null){
				carro.setIdProducto(peticion.getParameter("idProducto"));
				carro.setCantProducto("1");
			}else{
				if(carro.getIdProducto().contains(peticion.getParameter("idProducto"))){
					String idProducts[]=carro.getIdProducto().split(" ");
					String cantProducts[]=carro.getCantProducto().split(" ");
					for(int cont=0;cont<idProducts.length;cont++){
						if(idProducts[cont].trim().equals(peticion.getParameter("idProducto").trim())){
							int cantPro=Integer.parseInt(cantProducts[cont].trim());
							cantPro+=1;
							cantProducts[cont]=""+cantPro;
						}
					}		
					carro.setCantProducto("");
					for(int cont=0;cont<cantProducts.length;cont++){
						if(carro.getCantProducto().trim().equals("")){
							carro.setCantProducto(cantProducts[cont]);
						}else{
							carro.setCantProducto(carro.getCantProducto()+" "+cantProducts[cont]);
						}
					}
				}else{
					carro.setIdProducto(carro.getIdProducto()+" "+peticion.getParameter("idProducto"));
					carro.setCantProducto(carro.getCantProducto()+" 1");
				}
			}
		}catch(Exception e){
			
		}
		despachador.forward(peticion, respuesta);
	}

}
