package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jarriaza.beans.Carrito;
import org.jarriaza.utilidades.CarritoUtilidad;

public class ServletDelProCart extends HttpServlet{

	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("ServletCompraCarrito.do?estado=false");
		int idProducto=Integer.parseInt(peticion.getParameter("idProducto"));		
		Carrito carro=CarritoUtilidad.instancia.NuevoCarrito(peticion.getParameter("estadoCarr"));
		
		String cantPro[]=carro.getCantProducto().split(" ");
		String idPro[]=carro.getIdProducto().split(" ");
		
		for(int cont=0;cont<idPro.length;cont++){
			if(Integer.parseInt(idPro[cont].trim())==idProducto){
				int cantidadRestada=Integer.parseInt(cantPro[cont]);
				if((cantidadRestada-1)>=0){
					cantidadRestada-=1;
					cantPro[cont]=""+cantidadRestada;
				}
			}
		}
		carro.setCantProducto("");
		for(int cont=0;cont<cantPro.length;cont++){
			if(carro.getCantProducto().trim().equals("")){
				carro.setCantProducto(cantPro[cont]);
			}else{
				carro.setCantProducto(carro.getCantProducto()+" "+cantPro[cont]);
			}
		}
		
		despachador.forward(peticion,respuesta);
	}
}
