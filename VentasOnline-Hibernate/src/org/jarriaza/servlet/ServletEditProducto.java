package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jarriaza.beans.Producto;
import org.jarriaza.db.Conexion;

public class ServletEditProducto extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws ServletException, IOException{
		doPost(peticion,respuesta);		
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador;
		try{
			System.out.println(peticion.getParameter("editar").equals("true"));
			despachador=peticion.getRequestDispatcher("index.jsp");
			Producto productoEdit=(Producto) Conexion.getInstancia().Buscar(Producto.class,Integer.parseInt(peticion.getParameter("idProducto")));
			productoEdit.setPrecio(Integer.parseInt(peticion.getParameter("txtPrecio")));
			productoEdit.setCantidadDisponible(Integer.parseInt(peticion.getParameter("txtCantidad")));
			productoEdit.setNombreProducto(peticion.getParameter("txtNombre"));
			productoEdit.setDescripcion(peticion.getParameter("txtDescripcion"));
			productoEdit.setRutaImagen(peticion.getParameter("txtImagen"));
			Conexion.getInstancia().modificar(productoEdit);
			peticion.setAttribute("estadoEdit","true");
		}catch(NullPointerException npe){
			despachador=peticion.getRequestDispatcher("producto/editar.jsp");
			Producto productoEdit=(Producto) Conexion.getInstancia().Buscar(Producto.class,Integer.parseInt(peticion.getParameter("idProducto")));
			peticion.setAttribute("idProducto",productoEdit.getIdProducto());
			peticion.setAttribute("precio",productoEdit.getPrecio());
			peticion.setAttribute("cantidad",productoEdit.getCantidadDisponible());
			peticion.setAttribute("nombre",productoEdit.getNombreProducto());
			peticion.setAttribute("descripcion",productoEdit.getDescripcion());
			peticion.setAttribute("imagen",productoEdit.getRutaImagen());
		}
		despachador.forward(peticion,respuesta);
	}
}
