package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jarriaza.beans.Producto;
import org.jarriaza.db.Conexion;

public class ServletDelProducto extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws ServletException, IOException{
		doPost(peticion,respuesta);		
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		Producto productoDel=(Producto) Conexion.getInstancia().Buscar(Producto.class,Integer.parseInt(peticion.getParameter("idProducto")));
		try{
			Conexion.getInstancia().eliminar(productoDel);
		}catch(Exception e){
			
		}
		despachador.forward(peticion,respuesta);
	}
}
