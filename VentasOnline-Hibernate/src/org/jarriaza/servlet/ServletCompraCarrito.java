package org.jarriaza.servlet;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jarriaza.beans.Carrito;
import org.jarriaza.beans.Producto;
import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;
import org.jarriaza.utilidades.CarritoUtilidad;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class ServletCompraCarrito extends HttpServlet{
	
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador;
		despachador=peticion.getRequestDispatcher("index.jsp");
		HttpSession miSesion=peticion.getSession(true);
		Usuario usr =(Usuario) miSesion.getAttribute("usuario");
		try{
			if(peticion.getParameter("estado").equals("false")){
				despachador=peticion.getRequestDispatcher("carrito/ver.jsp");
				
				peticion.setAttribute("username",usr.getUsername());
				ArrayList<Producto> lista=new ArrayList<Producto>();
				Carrito carro=CarritoUtilidad.instancia.NuevoCarrito(peticion.getParameter("estadoCarr"));
				String listaPro[]=carro.getIdProducto().split(" ");
				String listaCant[]=carro.getCantProducto().split(" ");
				int precioTotal=0;
				for(int contas=0;contas<listaPro.length;contas++){
					if(Integer.parseInt(listaCant[contas].trim())>0){
						Producto proAdd=(Producto) Conexion.getInstancia().Buscar(Producto.class,Integer.parseInt(listaPro[contas].trim()));				
						proAdd.setCantidadDisponible(Integer.parseInt(listaCant[contas].trim()));
						precioTotal+=proAdd.getCantidadDisponible()*proAdd.getPrecio();
						lista.add(proAdd);
					}
				}
				peticion.setAttribute("precioTotal",precioTotal);
				peticion.setAttribute("lista",lista);
			}else{
				try{
					Carrito carro=CarritoUtilidad.instancia.NuevoCarrito(peticion.getParameter("estadoCarr"));
					File file = new File(usr.getUsername()+""+usr.getIdUsuario()+CarritoUtilidad.instancia.lastId()+""+".pdf");
					//String descuento=ManejadorProducto.descuento;
					String descuento="";
					FileOutputStream archivo = new FileOutputStream(file);
					String ruta=file.getAbsolutePath();
				    Document documento = new Document();
				    PdfWriter.getInstance(documento, archivo);
				    documento.open();
				    int total=0;
				    String listaP[]=carro.getIdProducto().split(" ");
				    String listaC[]=carro.getCantProducto().split(" ");
			    	documento.add(new Paragraph("Nombre :                                              "+usr.getNombre()));
			    	documento.add(new Paragraph("Tarjeta Credito :                                    "+usr.getTarjetaCredito()));
				    for(int cont=0;cont<listaP.length;cont++){
				    	documento.add(new Paragraph("_______________________________________________________________________"));
				    	documento.add(new Paragraph("                  "));
				    	documento.add(new Paragraph("Producto :                                            "+((Producto) Conexion.getInstancia().Buscar(Producto.class,Integer.parseInt(listaP[cont].trim()))).getNombreProducto()));
				    	documento.add(new Paragraph("Precio Unitario :                                   "+((Producto) Conexion.getInstancia().Buscar(Producto.class,Integer.parseInt(listaP[cont].trim()))).getPrecio()));
				    	documento.add(new Paragraph("Cantidad :                                            "+listaC[cont]));
				    	int cant=Integer.parseInt(listaC[cont].trim());
				    	int precio=((Producto) Conexion.getInstancia().Buscar(Producto.class,Integer.parseInt(listaP[cont].trim()))).getPrecio();
				    	total+=cant*precio;
				    }
				    if(descuento.contains("10")){
				    	total=(total*90)/100;
				    }else if(descuento.contains("15")){
				    	total=(total*85)/100;
				    }else if(descuento.contains("%5")){
				    	total=(total*95)/100;
				    }
				    documento.add(new Paragraph("                    "));
				    documento.add(new Paragraph("                     "));
				    documento.add(new Paragraph("_______________________________________________________________________"));
				    documento.add(new Paragraph("Total :                                                 "+total));
				    documento.close();
				    try {
			            File objetofile = new File (ruta);
			            Desktop.getDesktop().open(objetofile);
				     }catch (IOException ex) {
				    	System.out.println(ex);	
				     }
				    peticion.setAttribute("estadoPDF","Se ha realizado la compra!!");
				    peticion.setAttribute("file", file);
				    carro.setIdCarrito(CarritoUtilidad.instancia.lastId());
				    carro.setPrecioTotal(total);
				    Conexion.getInstancia().agregar(carro);
				    CarritoUtilidad.carro=null;
				}catch(Exception e){
					System.out.println("Excepcion");
				}
			}
		}catch(Exception e){
			
		}
		despachador.forward(peticion, respuesta);
	}

}
