package org.jarriaza.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;

public class ServletEditUsuario extends HttpServlet{

	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		Usuario userEdit=(Usuario) Conexion.getInstancia().Buscar(Usuario.class,Integer.parseInt(peticion.getParameter("idUsuario")));
		RequestDispatcher despachador;
		try{
			System.out.println(peticion.getParameter("editar").equals("true"));
			despachador=peticion.getRequestDispatcher("index.jsp");
			userEdit.setIdUsuario(Integer.parseInt(peticion.getParameter("idUsuario")));
			userEdit.setPassword(peticion.getParameter("txtPass"));		
			userEdit.setNombre(peticion.getParameter("txtNombre"));
			userEdit.setTarjetaCredito(peticion.getParameter("txtTarjeta"));
			userEdit.setImagen(peticion.getParameter("txtImagen"));
			Conexion.getInstancia().modificar(userEdit);
			peticion.setAttribute("estadoDel","");
		}catch(NullPointerException npe){
			despachador=peticion.getRequestDispatcher("usuario/editar.jsp");
			peticion.setAttribute("idUsuario",userEdit.getIdUsuario());
			peticion.setAttribute("username",userEdit.getUsername());
			peticion.setAttribute("nombre",userEdit.getNombre());
			peticion.setAttribute("tarjeta",userEdit.getTarjetaCredito());
			peticion.setAttribute("imagen",userEdit.getImagen());
		}
		despachador.forward(peticion,respuesta);
	}
	
}
