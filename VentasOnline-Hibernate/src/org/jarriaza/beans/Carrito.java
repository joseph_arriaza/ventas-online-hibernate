package org.jarriaza.beans;

public class Carrito {
	private int idCarrito,precioTotal;
	private Usuario usuario;
	private String idProducto,cantProducto;
	public int getIdCarrito() {
		return idCarrito;
	}
	public void setIdCarrito(int idCarrito) {
		this.idCarrito = idCarrito;
	}
	
	
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public int getPrecioTotal() {
		return precioTotal;
	}
	public void setPrecioTotal(int precioTotal) {
		this.precioTotal = precioTotal;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public String getCantProducto() {
		return cantProducto;
	}
	public void setCantProducto(String cantProducto) {
		this.cantProducto = cantProducto;
	}
	public Carrito() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Carrito(int idCarrito, int precioTotal, Usuario usuario,
			String idProducto, String cantProducto) {
		super();
		this.idCarrito = idCarrito;
		this.precioTotal = precioTotal;
		this.usuario = usuario;
		this.idProducto = idProducto;
		this.cantProducto = cantProducto;
	}

}
