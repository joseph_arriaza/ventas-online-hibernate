package org.jarriaza.beans;
public class Usuario {
	private Integer idUsuario;
	private String username, password, nombre, tarjetaCredito,imagen;
	private String rol;
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTarjetaCredito() {
		return tarjetaCredito;
	}
	public void setTarjetaCredito(String tarjetaCredito) {
		this.tarjetaCredito = tarjetaCredito;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol= rol;
		/*if(rolSend.contains("Admin")){
			
		}else{
			this.rol=Rol.Usuario;
		}*/
	}
	
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public Usuario() {
		super();
	}
	public Usuario(Integer idUsuario, String username, String password,
			String nombre, String tarjetaCredito, String imagen, String rol) {
		super();
		this.idUsuario = idUsuario;
		this.username = username;
		this.password = password;
		this.nombre = nombre;
		this.tarjetaCredito = tarjetaCredito;
		this.imagen = imagen;
		this.rol= rol;
	}
	
}
