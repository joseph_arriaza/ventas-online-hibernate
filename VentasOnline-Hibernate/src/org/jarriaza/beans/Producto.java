package org.jarriaza.beans;

public class Producto {
	private Integer idProducto,precio,cantidadDisponible;
	private String nombreProducto,descripcion,rutaImagen;
	public Integer getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	public Integer getPrecio() {
		return precio;
	}
	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
	public Integer getCantidadDisponible() {
		return cantidadDisponible;
	}
	public void setCantidadDisponible(Integer cantidadDisponible) {
		this.cantidadDisponible = cantidadDisponible;
	}
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getRutaImagen() {
		return rutaImagen;
	}
	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}
	public Producto() {
		super();
	}
	public Producto(Integer idProducto, Integer precio,
			Integer cantidadDisponible, String nombreProducto,
			String descripcion, String rutaImagen) {
		super();
		this.idProducto = idProducto;
		this.precio = precio;
		this.cantidadDisponible = cantidadDisponible;
		this.nombreProducto = nombreProducto;
		this.descripcion = descripcion;
		this.rutaImagen = rutaImagen;
	}
	
	
}
